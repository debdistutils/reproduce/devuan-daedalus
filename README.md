# Reproducible builds of Devuan GNU+Linux 5.0 Daedalus

This project provides [reproducible
build](https://reproducible-builds.org/) status for
[Devuan GNU+Linux 5.0 Daedalus](https://www.devuan.org/) on
amd64.

## Status

We have reproducibly built **42%** of the
difference between **Devuan GNU+Linux 5.0 Daedalus** and
**Debian 12 Bookworm**!  That is **42%** of
the packages we have built, and we have built **100%** or
**88** of the **88** source packages to rebuild.

Devuan GNU+Linux 5.0 Daedalus (on amd64) contains binary packages
that were added/modified compared to what is in
Debian 12 Bookworm (on amd64) that corresponds to
**86** source packages.  Some binary packages exists in more
than one version, so there is a total of **88** source packages to
rebuild.  Of these we have built **37**
reproducibly out of the **88** builds so far.

We have build logs for **94** builds, which may exceed the number
of total source packages to rebuild when a particular source package
(or source package version) has been removed from the archive.  Of the
packages we built, **37** packages are reproducible and
there are **43** packages that we could not reproduce.
Building **14** package had build failures.  We do not attempt to
build **0** packages.

[[_TOC_]]

### Unreproducible packages

The following **43** packages unfortunately differ in
some way compared to the version distributed in the archive.  Please
investigate the build log and [diffoscope](https://diffoscope.org)
output and help us fix it!

| Package | Version | Diffoscope output | Link to build log |
| ------- | ------- | ----------------- | ----------------- |
| 389-ds-base | 1.4.4.11-2+devuan0.1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/389-ds-base/1.4.4.11-2+devuan0.1/index.html) | [build log from Wed Nov 29 08:42:29 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/389-ds-base/1.4.4.11-2+devuan0.1/buildlog.txt) |
| acdcli | 0.3.2-10 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/acdcli/0.3.2-10/index.html) | [build log from Wed Nov 29 08:55:19 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/acdcli/0.3.2-10/buildlog.txt) |
| apt | 2.6.1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/apt/2.6.1devuan1/index.html) | [build log from Wed Nov 29 08:44:06 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/apt/2.6.1devuan1/buildlog.txt) |
| cgroupfs-mount | 1.4+devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/cgroupfs-mount/1.4+devuan1/index.html) | [build log from Wed Nov 29 09:01:37 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/cgroupfs-mount/1.4+devuan1/buildlog.txt) |
| colord | 1.4.6-2.2devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/colord/1.4.6-2.2devuan1/index.html) | [build log from Wed Nov 29 08:46:42 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/colord/1.4.6-2.2devuan1/buildlog.txt) |
| consolekit2 | 1.2.4-2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/consolekit2/1.2.4-2/index.html) | [build log from Wed Nov 29 09:05:05 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/consolekit2/1.2.4-2/buildlog.txt) |
| cryptsetup-modified-functions | 23.02.12 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/cryptsetup-modified-functions/23.02.12/index.html) | [build log from Wed Nov 29 09:02:03 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/cryptsetup-modified-functions/23.02.12/buildlog.txt) |
| czmq | 4.2.1-1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/czmq/4.2.1-1devuan1/index.html) | [build log from Wed Nov 29 08:58:12 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/czmq/4.2.1-1devuan1/buildlog.txt) |
| elogind | 246.10-5 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/elogind/246.10-5/index.html) | [build log from Wed Nov 29 09:03:26 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/elogind/246.10-5/buildlog.txt) |
| fluidsynth | 2.3.1-2devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/fluidsynth/2.3.1-2devuan1/index.html) | [build log from Wed Nov 29 09:06:01 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/fluidsynth/2.3.1-2devuan1/buildlog.txt) |
| gst-plugins-bad1.0 | 1.22.0-4+deb12u2 | no diffoscope output (probably too large) | [build log from Sun Nov 26 12:52:53 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/gst-plugins-bad1.0/1.22.0-4+deb12u2/buildlog.txt) |
| hylafax | 3:6.0.7-5devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/hylafax/3:6.0.7-5devuan1/index.html) | [build log from Wed Nov 29 09:07:52 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/hylafax/3:6.0.7-5devuan1/buildlog.txt) |
| init-system-helpers | 1.65.2devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/init-system-helpers/1.65.2devuan1/index.html) | [build log from Wed Nov 29 09:13:46 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/init-system-helpers/1.65.2devuan1/buildlog.txt) |
| ldm | 2:2.18.06-1+devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/ldm/2:2.18.06-1+devuan1/index.html) | [build log from Wed Nov 29 09:08:07 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/ldm/2:2.18.06-1+devuan1/buildlog.txt) |
| libvirt | 9.0.0-4devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/libvirt/9.0.0-4devuan1/index.html) | [build log from Wed Nov 29 09:19:50 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/libvirt/9.0.0-4devuan1/buildlog.txt) |
| lightdm | 1.26.0-8+devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/lightdm/1.26.0-8+devuan1/index.html) | [build log from Wed Nov 29 09:10:19 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/lightdm/1.26.0-8+devuan1/buildlog.txt) |
| live-build | 4.0.3-1+devuan2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/live-build/4.0.3-1+devuan2/index.html) | [build log from Wed Nov 29 09:14:28 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/live-build/4.0.3-1+devuan2/buildlog.txt) |
| live-config | 11.0.3+nmu1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/live-config/11.0.3+nmu1devuan1/index.html) | [build log from Wed Nov 29 09:29:20 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/live-config/11.0.3+nmu1devuan1/buildlog.txt) |
| ltsp | 5.18.12-3+devuan2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/ltsp/5.18.12-3+devuan2/index.html) | [build log from Wed Nov 29 09:15:31 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/ltsp/5.18.12-3+devuan2/buildlog.txt) |
| net-tools | 2.10-0.1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/net-tools/2.10-0.1devuan1/index.html) | [build log from Wed Nov 29 09:20:28 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/net-tools/2.10-0.1devuan1/buildlog.txt) |
| network-manager | 1.42.4-1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/network-manager/1.42.4-1devuan1/index.html) | [build log from Wed Nov 29 09:16:36 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/network-manager/1.42.4-1devuan1/buildlog.txt) |
| oddjob | 0.34.7-1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/oddjob/0.34.7-1devuan1/index.html) | [build log from Wed Nov 29 09:34:38 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/oddjob/0.34.7-1devuan1/buildlog.txt) |
| openvpn | 2.6.3-2devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/openvpn/2.6.3-2devuan1/index.html) | [build log from Wed Nov 29 09:22:32 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/openvpn/2.6.3-2devuan1/buildlog.txt) |
| openvpn | 2.6.3-2devuan2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/openvpn/2.6.3-2devuan2/index.html) | [build log from Wed Nov 29 18:52:13 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/openvpn/2.6.3-2devuan2/buildlog.txt) |
| pcsc-lite | 1.9.9-2devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/pcsc-lite/1.9.9-2devuan1/index.html) | [build log from Wed Nov 29 09:30:03 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/pcsc-lite/1.9.9-2devuan1/buildlog.txt) |
| pdns-recursor | 4.8.4-1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/pdns-recursor/4.8.4-1devuan1/index.html) | [build log from Wed Nov 29 09:23:58 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/pdns-recursor/4.8.4-1devuan1/buildlog.txt) |
| pinthread | 0.4 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/pinthread/0.4/index.html) | [build log from Wed Nov 29 09:41:24 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/pinthread/0.4/buildlog.txt) |
| plymouth | 22.02.122-3devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/plymouth/22.02.122-3devuan1/index.html) | [build log from Wed Nov 29 09:31:00 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/plymouth/22.02.122-3devuan1/buildlog.txt) |
| popularity-contest | 1.76devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/popularity-contest/1.76devuan1/index.html) | [build log from Wed Nov 29 09:35:24 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/popularity-contest/1.76devuan1/buildlog.txt) |
| procps | 2:4.0.3-1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/procps/2:4.0.3-1devuan1/index.html) | [build log from Wed Nov 29 09:31:58 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/procps/2:4.0.3-1devuan1/buildlog.txt) |
| python-apt-common-devuan | 2.5.3devuan2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/python-apt-common-devuan/2.5.3devuan2/index.html) | [build log from Wed Nov 29 10:22:23 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/python-apt-common-devuan/2.5.3devuan2/buildlog.txt) |
| rrqnet | 1.6.3 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/rrqnet/1.6.3/index.html) | [build log from Wed Nov 29 09:32:10 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/rrqnet/1.6.3/buildlog.txt) |
| rsyslog | 8.2302.0-1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/rsyslog/8.2302.0-1devuan1/index.html) | [build log from Wed Nov 29 09:36:35 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/rsyslog/8.2302.0-1devuan1/buildlog.txt) |
| sgm | 0.90.0-2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/sgm/0.90.0-2/index.html) | [build log from Wed Nov 29 09:42:45 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/sgm/0.90.0-2/buildlog.txt) |
| slim | 1.4.0-0devuan2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/slim/1.4.0-0devuan2/index.html) | [build log from Wed Nov 29 09:37:41 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/slim/1.4.0-0devuan2/buildlog.txt) |
| sshguard | 2.4.2-1+devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/sshguard/2.4.2-1+devuan1/index.html) | [build log from Wed Nov 29 10:32:55 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/sshguard/2.4.2-1+devuan1/buildlog.txt) |
| tomcat9 | 9.0.70-1devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/tomcat9/9.0.70-1devuan1/index.html) | [build log from Wed Nov 29 09:43:11 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/tomcat9/9.0.70-1devuan1/buildlog.txt) |
| udev | 1:3.2.9+devuan4 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/udev/1:3.2.9+devuan4/index.html) | [build log from Wed Nov 29 10:23:27 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/udev/1:3.2.9+devuan4/buildlog.txt) |
| udisks2 | 2.9.4-4devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/udisks2/2.9.4-4devuan1/index.html) | [build log from Wed Nov 29 09:44:15 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/udisks2/2.9.4-4devuan1/buildlog.txt) |
| util-linux | 2.38.1-5devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/util-linux/2.38.1-5devuan1/index.html) | [build log from Wed Nov 29 10:38:42 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/util-linux/2.38.1-5devuan1/buildlog.txt) |
| xlennart | 1.1.1-devuan | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/xlennart/1.1.1-devuan/index.html) | [build log from Wed Nov 29 09:53:08 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/xlennart/1.1.1-devuan/buildlog.txt) |
| xorg-server | 2:21.1.7-3+deb12u2devuan1 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/xorg-server/2:21.1.7-3+deb12u2devuan1/index.html) | [build log from Wed Nov 29 18:51:45 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/xorg-server/2:21.1.7-3+deb12u2devuan1/buildlog.txt) |
| xorg-server | 2:21.1.7-3devuan2 | [diffoscope output](https://devuan-daedalus-debdistutils-reproduce-4a43e91227ee58f68192f675.gitlab.io/diffoscope/xorg-server/2:21.1.7-3devuan2/index.html) | [build log from Wed Nov 29 10:23:34 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/xorg-server/2:21.1.7-3devuan2/buildlog.txt) |

### Build failures

The following **14** packages have build failures, making it
impossible to even compare the binary package in the archive with what
we are able to build locally.

#### Missing source code

The archive is missing source code for the following source packages,
for which the archive is shipping a binary packages that claims it was
built using that particular source package/version.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
| amd64-microcode | 3.20230719.1~deb12u1 | [build log from Sun Nov 26 11:51:06 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/amd64-microcode/3.20230719.1~deb12u1/buildlog.txt) |
| intel-microcode | 3.20231114.1~deb12u1 | [build log from Sun Nov 26 12:53:53 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/intel-microcode/3.20231114.1~deb12u1/buildlog.txt) |

#### Other build failures

Please investigate the build log and help us fix it!

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
| cgmanager | 0.41-2+devuan1 | [build log from Sun Nov 26 12:06:31 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/cgmanager/0.41-2+devuan1/buildlog.txt) |
| cmake | 3.27.7-1~bpo12+1 | [build log from Mon Nov 27 07:17:22 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/cmake/3.27.7-1~bpo12+1/buildlog.txt) |
| dbus | 1.14.10-1~deb12u1devuan1 | [build log from Sun Nov 26 12:21:42 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/dbus/1.14.10-1~deb12u1devuan1/buildlog.txt) |
| debian-installer | 20230607+deb12u2devuan1 | [build log from Sun Nov 26 12:29:54 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/debian-installer/20230607+deb12u2devuan1/buildlog.txt) |
| devuan-keyring | 2023.05.28 | [build log from Sun Nov 26 12:51:21 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/devuan-keyring/2023.05.28/buildlog.txt) |
| dnscrypt-proxy | 2.0.45+ds1-1+devuan1 | [build log from Sun Nov 26 12:46:07 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/dnscrypt-proxy/2.0.45+ds1-1+devuan1/buildlog.txt) |
| gpsd | 3.22-4.1devuan1 | [build log from Sun Nov 26 12:48:30 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/gpsd/3.22-4.1devuan1/buildlog.txt) |
| igraph | 0.10.7+ds-1~bpo12+1 | [build log from Sun Nov 26 12:53:06 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/igraph/0.10.7+ds-1~bpo12+1/buildlog.txt) |
| ltspfs | 1.5-2+devuan1 | [build log from Sun Nov 26 14:52:20 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/ltspfs/1.5-2+devuan1/buildlog.txt) |
| python-nbxmpp | 4.5.2-1~bpo12+1 | [build log from Mon Nov 27 07:22:22 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/python-nbxmpp/4.5.2-1~bpo12+1/buildlog.txt) |
| thunderbird | 1:115.4.1-1~deb12u1 | [build log from Wed Nov 29 09:39:51 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/thunderbird/1:115.4.1-1~deb12u1/buildlog.txt) |
| trac | 1.6-1~bpo12+1 | [build log from Mon Nov 27 07:17:35 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/trac/1.6-1~bpo12+1/buildlog.txt) |

### Reproducible packages

The following **37** packages can be built locally to
produce the exact same package that is shipped in the archive.

| Package | Version | Link to build log |
| ------- | ------- | ----------------- |
| avahi | 0.8-10devuan1 | [build log from Sun Nov 26 11:54:12 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/avahi/0.8-10devuan1/buildlog.txt) |
| base-files | 12.4devuan3 | [build log from Sun Nov 26 11:51:57 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/base-files/12.4devuan3/buildlog.txt) |
| cinnabar-icon-theme | 1.1 | [build log from Sun Nov 26 12:18:54 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/cinnabar-icon-theme/1.1/buildlog.txt) |
| clearlooks-phenix-cinnabar-theme | 7.0.1-5 | [build log from Wed Nov 29 08:37:31 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/clearlooks-phenix-cinnabar-theme/7.0.1-5/buildlog.txt) |
| clearlooks-phenix-darkpurpy-theme | 7.0.2-1+devuan3.0 | [build log from Wed Nov 29 08:44:17 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/clearlooks-phenix-darkpurpy-theme/7.0.2-1+devuan3.0/buildlog.txt) |
| clearlooks-phenix-deepsea-theme | 10.0-2 | [build log from Wed Nov 29 08:58:00 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/clearlooks-phenix-deepsea-theme/10.0-2/buildlog.txt) |
| clearlooks-phenix-lightpurpy-theme | 7.0.1-4 | [build log from Wed Nov 29 08:45:37 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/clearlooks-phenix-lightpurpy-theme/7.0.1-4/buildlog.txt) |
| clearlooks-phenix-sapphire-theme | 10.0.0-2 | [build log from Wed Nov 29 08:56:49 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/clearlooks-phenix-sapphire-theme/10.0.0-2/buildlog.txt) |
| darkpurpy-icon-theme | 1.1 | [build log from Sun Nov 26 12:15:14 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/darkpurpy-icon-theme/1.1/buildlog.txt) |
| debian-config-override | 2.6 | [build log from Sun Nov 26 12:20:30 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/debian-config-override/2.6/buildlog.txt) |
| debootstrap | 1.0.128+nmu2devuan2 | [build log from Sun Nov 26 12:45:59 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/debootstrap/1.0.128+nmu2devuan2/buildlog.txt) |
| deepsea-icon-theme | 1.1 | [build log from Wed Nov 29 09:12:33 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/deepsea-icon-theme/1.1/buildlog.txt) |
| desktop-base | 1:5.5 | [build log from Sun Nov 26 12:31:07 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/desktop-base/1:5.5/buildlog.txt) |
| devscripts-devuan | 0.3 | [build log from Sun Nov 26 12:35:46 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/devscripts-devuan/0.3/buildlog.txt) |
| devuan-baseconf | 0.6.4+devuan3.2 | [build log from Wed Nov 29 08:59:38 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/devuan-baseconf/0.6.4+devuan3.2/buildlog.txt) |
| devuan-lintian-profile | 1.11 | [build log from Sun Nov 26 12:33:00 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/devuan-lintian-profile/1.11/buildlog.txt) |
| distro-info-data | 0.58devuan1 | [build log from Sun Nov 26 12:37:05 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/distro-info-data/0.58devuan1/buildlog.txt) |
| dq | 20230101-1devuan3 | [build log from Sun Nov 26 12:37:10 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/dq/20230101-1devuan3/buildlog.txt) |
| eudev | 3.2.12-4+deb12u1 | [build log from Sun Nov 26 12:38:50 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/eudev/3.2.12-4+deb12u1/buildlog.txt) |
| firewalld | 1.3.3-1~deb12u1devuan1 | [build log from Sun Nov 26 12:47:46 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/firewalld/1.3.3-1~deb12u1devuan1/buildlog.txt) |
| fontsnaps | 1.4 | [build log from Wed Nov 29 09:03:52 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/fontsnaps/1.4/buildlog.txt) |
| freeipa | 4.9.11-2devuan1 | [build log from Sun Nov 26 13:29:28 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/freeipa/4.9.11-2devuan1/buildlog.txt) |
| jenkins-buildenv-devuan | 1.3 | [build log from Sun Nov 26 13:30:50 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/jenkins-buildenv-devuan/1.3/buildlog.txt) |
| jenkins-debian-glue-buildenv-devuan | 1.0 | [build log from Sun Nov 26 13:52:42 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/jenkins-debian-glue-buildenv-devuan/1.0/buildlog.txt) |
| packagekit | 1.2.6-5devuan1 | [build log from Sun Nov 26 20:39:54 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/packagekit/1.2.6-5devuan1/buildlog.txt) |
| pam-mkhomedir | 1.0-1 | [build log from Sun Nov 26 14:42:54 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/pam-mkhomedir/1.0-1/buildlog.txt) |
| policykit-1 | 122-3devuan1 | [build log from Sun Nov 26 14:53:21 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/policykit-1/122-3devuan1/buildlog.txt) |
| refractainstaller-base | 9.7.0 | [build log from Sun Nov 26 14:56:24 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/refractainstaller-base/9.7.0/buildlog.txt) |
| refractainstaller-gui | 9.7.0 | [build log from Sun Nov 26 15:00:51 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/refractainstaller-gui/9.7.0/buildlog.txt) |
| refractasnapshot-base | 10.3.0 | [build log from Sun Nov 26 15:06:00 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/refractasnapshot-base/10.3.0/buildlog.txt) |
| refractasnapshot-gui | 10.3.0 | [build log from Sun Nov 26 15:00:56 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/refractasnapshot-gui/10.3.0/buildlog.txt) |
| reportbug | 12.0.0devuan2 | [build log from Sun Nov 26 16:22:41 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/reportbug/12.0.0devuan2/buildlog.txt) |
| systemctl-service-shim | 0.0.5-1 | [build log from Sun Nov 26 16:34:23 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/systemctl-service-shim/0.0.5-1/buildlog.txt) |
| sysvinit | 3.06-4devuan3 | [build log from Sun Nov 26 15:24:45 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/sysvinit/3.06-4devuan3/buildlog.txt) |
| tasksel | 3.73devuan1 | [build log from Sun Nov 26 16:22:49 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/tasksel/3.73devuan1/buildlog.txt) |
| unattended-upgrades | 2.9.1+nmu3 | [build log from Sun Nov 26 16:35:07 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/unattended-upgrades/2.9.1+nmu3/buildlog.txt) |
| xfce4-session | 4.18.1-1devuan2 | [build log from Sun Nov 26 18:33:33 UTC 2023](https://gitlab.com/debdistutils/reproduce/devuan-daedalus/-/blob/main/logs/xfce4-session/4.18.1-1devuan2/buildlog.txt) |

### Timestamps

The timestamps of the archives used as input to find out which
packages to reproduce are as follows.  To be precise, these are the
`Date` field in the respectively `Release` file used to construct
the list of packages to evaluate.

When speaking about "current" status of this effort it makes sense to
use the latest timestamp from the set below, which is
**Wed Nov 29 14:43:02 UTC 2023**.

| Suite | Debian 12 Bookworm | Devuan GNU+Linux 5.0 Daedalus |
| ----- | ------------------------------ | -------------------------------- |
| bookworm / daedalus | Sat, 07 Oct 2023 09:29:19 UTC | Tue, 28 Nov 2023 17:44:18 UTC |
| bookworm-updates / daedalus-updates | Wed, 29 Nov 2023 14:11:14 UTC | Wed, 29 Nov 2023 14:43:02 UTC |
| bookworm-security / daedalus-security | Wed, 29 Nov 2023 01:25:04 UTC | Wed, 29 Nov 2023 01:28:14 UTC |
| bookworm-backports / daedalus-backports | Wed, 29 Nov 2023 14:11:14 UTC | Wed, 29 Nov 2023 14:42:46 UTC |

## License

This repository is updated automatically by
[debdistreproduce](https://gitlab.com/debdistutils/debdistreproduce)
but to the extent anything is copyrightable, everything is published
under the AGPLv3+ see the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html].

## Contact

The maintainer of this project is [Simon
Josefsson](https://blog.josefsson.org/).
