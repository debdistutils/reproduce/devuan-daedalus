+ date
Sun Nov 26 12:18:54 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source cinnabar-icon-theme=1.1
Reading package lists...
Need to get 882 kB of source archives.
Get:1 http://deb.devuan.org/merged daedalus/main cinnabar-icon-theme 1.1 (dsc) [1071 B]
Get:2 http://deb.devuan.org/merged daedalus/main cinnabar-icon-theme 1.1 (tar) [881 kB]
dpkg-source: info: extracting cinnabar-icon-theme in cinnabar-icon-theme-1.1
dpkg-source: info: unpacking cinnabar-icon-theme_1.1.tar.gz
Fetched 882 kB in 1s (1729 kB/s)
W: Download is performed unsandboxed as root as file 'cinnabar-icon-theme_1.1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source cinnabar-icon-theme=1.1
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name cinnabar-icon-theme* -type d
+ cd ./cinnabar-icon-theme-1.1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package cinnabar-icon-theme
dpkg-buildpackage: info: source version 1.1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building cinnabar-icon-theme in cinnabar-icon-theme_1.1.tar.gz
dpkg-source: warning: missing information for output field Standards-Version
dpkg-source: info: building cinnabar-icon-theme in cinnabar-icon-theme_1.1.dsc
 debian/rules build
make: Nothing to be done for 'build'.
 debian/rules binary
dh_testdir
dh_testroot
dh_clean -k
dh_clean: warning: dh_clean -k is deprecated; use dh_prep instead
dh_clean: warning: This feature will be removed in compat 12.
mkdir -p debian/cinnabar-icon-theme
cp -ra files/* debian/cinnabar-icon-theme
dh_installdocs -pcinnabar-icon-theme
dh_installchangelogs -pcinnabar-icon-theme
dh_compress
dh_fixperms
dh_strip
dh_makeshlibs
dh_shlibdeps
dh_installdeb
dh_gencontrol
dh_md5sums
dh_builddeb
dpkg-deb: building package 'cinnabar-icon-theme' in '../cinnabar-icon-theme_1.1_all.deb'.
 dpkg-genbuildinfo -O../cinnabar-icon-theme_1.1_amd64.buildinfo
 dpkg-genchanges -O../cinnabar-icon-theme_1.1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Nov 26 12:19:00 UTC 2023
+ cd ..
+ ls -la
total 1612
drwxr-xr-x 3 root root   4096 Nov 26 12:19 .
drwxr-xr-x 3 root root   4096 Nov 26 12:18 ..
-rw-r--r-- 1 root root   2809 Nov 26 12:19 buildlog.txt
drwxr-xr-x 4 root root   4096 Aug 16  2021 cinnabar-icon-theme-1.1
-rw-r--r-- 1 root root    533 Nov 26 12:18 cinnabar-icon-theme_1.1.dsc
-rw-r--r-- 1 root root 885664 Nov 26 12:18 cinnabar-icon-theme_1.1.tar.gz
-rw-r--r-- 1 root root 727356 Nov 26 12:18 cinnabar-icon-theme_1.1_all.deb
-rw-r--r-- 1 root root   5072 Nov 26 12:19 cinnabar-icon-theme_1.1_amd64.buildinfo
-rw-r--r-- 1 root root   1587 Nov 26 12:19 cinnabar-icon-theme_1.1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./cinnabar-icon-theme_1.1_amd64.changes ./cinnabar-icon-theme_1.1.dsc ./cinnabar-icon-theme_1.1_amd64.buildinfo ./cinnabar-icon-theme_1.1.tar.gz ./cinnabar-icon-theme_1.1_all.deb
57ea859540d20782fc20a935fe07fa1171e4ebb5d5d127cf08bb1c178a4ea69d  ./buildlog.txt
724ed8d1a9aa86a8b4d48e2b61b11ca2e82f02cff9cf1d71c0dfff12d353d0ef  ./cinnabar-icon-theme_1.1_amd64.changes
a2bbf70c62bd07e31abd2294eea53821d3ca9bb11aa5455160b02164cf2227af  ./cinnabar-icon-theme_1.1.dsc
757f8d41d2d6a900b220d28ebfa6876e4eeff84d9bd506b7bf5f20578b0f23ab  ./cinnabar-icon-theme_1.1_amd64.buildinfo
29ac062b1c00ba9e2fc64adadbff46bef4fd63d6324cabf5352ed8499e8957b3  ./cinnabar-icon-theme_1.1.tar.gz
982f551296aec3f44647646074d0996c7ff0a9479ca6376c4f23047c6ef7c638  ./cinnabar-icon-theme_1.1_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls cinnabar-icon-theme_1.1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://deb.devuan.org/merged/pool/DEVUAN/main/c/cinnabar-icon-theme/cinnabar-icon-theme_1.1_all.deb
--2023-11-26 12:19:00--  http://deb.devuan.org/merged/pool/DEVUAN/main/c/cinnabar-icon-theme/cinnabar-icon-theme_1.1_all.deb
Resolving deb.devuan.org (deb.devuan.org)... 160.16.137.156, 131.188.12.211, 195.85.215.180, ...
Connecting to deb.devuan.org (deb.devuan.org)|160.16.137.156|:80... connected.
HTTP request sent, awaiting response... 301 Moved Permanently
Location: /devuan/pool/main/c/cinnabar-icon-theme/cinnabar-icon-theme_1.1_all.deb [following]
--2023-11-26 12:19:01--  http://deb.devuan.org/devuan/pool/main/c/cinnabar-icon-theme/cinnabar-icon-theme_1.1_all.deb
Reusing existing connection to deb.devuan.org:80.
HTTP request sent, awaiting response... 200 OK
Length: 727356 (710K) [application/vnd.debian.binary-package]
Saving to: 'cinnabar-icon-theme_1.1_all.deb'

     0K .......... .......... .......... .......... ..........  7% 85.3K 8s
    50K .......... .......... .......... .......... .......... 14%  171K 5s
   100K .......... .......... .......... .......... .......... 21% 20.8M 3s
   150K .......... .......... .......... .......... .......... 28% 26.0M 2s
   200K .......... .......... .......... .......... .......... 35%  172K 2s
   250K .......... .......... .......... .......... .......... 42% 23.0M 2s
   300K .......... .......... .......... .......... .......... 49%  165K 2s
   350K .......... .......... .......... .......... .......... 56% 16.7M 1s
   400K .......... .......... .......... .......... .......... 63% 16.0M 1s
   450K .......... .......... .......... .......... .......... 70% 18.8M 1s
   500K .......... .......... .......... .......... .......... 77% 19.3M 0s
   550K .......... .......... .......... .......... .......... 84% 19.9M 0s
   600K .......... .......... .......... .......... .......... 91% 15.1M 0s
   650K .......... .......... .......... .......... .......... 98%  184K 0s
   700K ..........                                            100% 8.44M=1.8s

2023-11-26 12:19:03 (401 KB/s) - 'cinnabar-icon-theme_1.1_all.deb' saved [727356/727356]

+ tee ../SHA256SUMS+ 
find . -maxdepth 1 -type f
+ sha256sum ./cinnabar-icon-theme_1.1_all.deb
982f551296aec3f44647646074d0996c7ff0a9479ca6376c4f23047c6ef7c638  ./cinnabar-icon-theme_1.1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./cinnabar-icon-theme_1.1_all.deb: OK
+ echo Package cinnabar-icon-theme version 1.1 is reproducible!
Package cinnabar-icon-theme version 1.1 is reproducible!
