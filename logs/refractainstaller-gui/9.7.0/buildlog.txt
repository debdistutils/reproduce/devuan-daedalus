+ date
Sun Nov 26 15:00:51 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source refractainstaller-gui=9.7.0
Reading package lists...
NOTICE: 'refractainstaller-gui' packaging is maintained in the 'Git' version control system at:
git@git.devuan.org:devuan/refractainstaller-gui.git
Please use:
git clone git@git.devuan.org:devuan/refractainstaller-gui.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 34.1 kB of source archives.
Get:1 http://deb.devuan.org/merged daedalus/main refractainstaller-gui 9.7.0 (dsc) [1266 B]
Get:2 http://deb.devuan.org/merged daedalus/main refractainstaller-gui 9.7.0 (tar) [32.9 kB]
dpkg-source: info: extracting refractainstaller-gui in refractainstaller-gui-9.7.0
dpkg-source: info: unpacking refractainstaller-gui_9.7.0.tar.xz
Fetched 34.1 kB in 1s (30.7 kB/s)
W: Download is performed unsandboxed as root as file 'refractainstaller-gui_9.7.0.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source refractainstaller-gui=9.7.0
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name refractainstaller-gui* -type d
+ cd ./refractainstaller-gui-9.7.0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package refractainstaller-gui
dpkg-buildpackage: info: source version 9.7.0
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/refractainstaller-yad"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/refractainstaller-wrapper.sh"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/move-dir-mount-gui.sh"
dh clean 
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building refractainstaller-gui in refractainstaller-gui_9.7.0.tar.xz
dpkg-source: info: building refractainstaller-gui in refractainstaller-gui_9.7.0.dsc
 debian/rules build
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/refractainstaller-yad"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/refractainstaller-wrapper.sh"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/move-dir-mount-gui.sh"
dh build 
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/refractainstaller-yad"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/refractainstaller-wrapper.sh"
chmod 0655 "/build/refractainstaller-gui/refractainstaller-gui-9.7.0/move-dir-mount-gui.sh"
dh binary 
   dh_testroot
   dh_prep
   dh_auto_install --destdir=debian/refractainstaller-gui/
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package refractainstaller-gui: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'refractainstaller-gui' in '../refractainstaller-gui_9.7.0_all.deb'.
 dpkg-genbuildinfo -O../refractainstaller-gui_9.7.0_amd64.buildinfo
 dpkg-genchanges -O../refractainstaller-gui_9.7.0_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Nov 26 15:00:59 UTC 2023
+ cd ..
+ ls -la
total 100
drwxr-xr-x 3 root root  4096 Nov 26 15:00 .
drwxr-xr-x 3 root root  4096 Nov 26 15:00 ..
-rw-r--r-- 1 root root  3938 Nov 26 15:00 buildlog.txt
drwxr-xr-x 3 root root  4096 Jun  7 20:19 refractainstaller-gui-9.7.0
-rw-r--r-- 1 root root   728 Nov 26 15:00 refractainstaller-gui_9.7.0.dsc
-rw-r--r-- 1 root root 32892 Nov 26 15:00 refractainstaller-gui_9.7.0.tar.xz
-rw-r--r-- 1 root root 31704 Nov 26 15:00 refractainstaller-gui_9.7.0_all.deb
-rw-r--r-- 1 root root  5105 Nov 26 15:00 refractainstaller-gui_9.7.0_amd64.buildinfo
-rw-r--r-- 1 root root  1655 Nov 26 15:00 refractainstaller-gui_9.7.0_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./refractainstaller-gui_9.7.0_all.deb ./refractainstaller-gui_9.7.0.dsc ./refractainstaller-gui_9.7.0_amd64.changes ./refractainstaller-gui_9.7.0.tar.xz ./refractainstaller-gui_9.7.0_amd64.buildinfo
b0895b6d0d170e4e051e45b880ea7fe29fa01dce4b45a58e34d3e67bdf165229  ./buildlog.txt
cbf16f49302306293cd193a8bfc0604b2a731121d243dc159ec1b8331b0227d5  ./refractainstaller-gui_9.7.0_all.deb
ae6ff3fcb3366f3666b6b4ef7a83e065b5d8b64e605aed4176d45537dcdb448b  ./refractainstaller-gui_9.7.0.dsc
bf813282c35531dbd3575501ab6db031464468da1c1cec140da0571737dd3ce6  ./refractainstaller-gui_9.7.0_amd64.changes
535693f0c1cf9d3731412faf534553d252a4c11b59089970c60e4af6825f3339  ./refractainstaller-gui_9.7.0.tar.xz
ed523346414c858d3ab63e8bbdb63c037cf34f9bfceb7f64b8e1859e82fc6440  ./refractainstaller-gui_9.7.0_amd64.buildinfo
+ mkdir published
+ cd published
+ cd ../
+ ls refractainstaller-gui_9.7.0_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://deb.devuan.org/merged/pool/DEVUAN/main/r/refractainstaller-gui/refractainstaller-gui_9.7.0_all.deb
--2023-11-26 15:00:59--  http://deb.devuan.org/merged/pool/DEVUAN/main/r/refractainstaller-gui/refractainstaller-gui_9.7.0_all.deb
Resolving deb.devuan.org (deb.devuan.org)... 46.4.50.2, 160.16.137.156, 200.236.31.1, ...
Connecting to deb.devuan.org (deb.devuan.org)|46.4.50.2|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 31704 (31K) [application/x-debian-package]
Saving to: 'refractainstaller-gui_9.7.0_all.deb'

     0K .......... .......... ..........                      100% 1.07M=0.03s

2023-11-26 15:00:59 (1.07 MB/s) - 'refractainstaller-gui_9.7.0_all.deb' saved [31704/31704]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./refractainstaller-gui_9.7.0_all.deb
cbf16f49302306293cd193a8bfc0604b2a731121d243dc159ec1b8331b0227d5  ./refractainstaller-gui_9.7.0_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./refractainstaller-gui_9.7.0_all.deb: OK
+ echo Package refractainstaller-gui version 9.7.0 is reproducible!
Package refractainstaller-gui version 9.7.0 is reproducible!
