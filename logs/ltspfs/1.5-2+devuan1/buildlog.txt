+ date
Sun Nov 26 14:52:20 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source ltspfs=1.5-2+devuan1
Reading package lists...
NOTICE: 'ltspfs' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/ltspfs.git
Please use:
git clone https://git.devuan.org/devuan/ltspfs.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 48.6 kB of source archives.
Get:1 http://deb.devuan.org/merged daedalus/main ltspfs 1.5-2+devuan1 (dsc) [1584 B]
Get:2 http://deb.devuan.org/merged daedalus/main ltspfs 1.5-2+devuan1 (tar) [38.1 kB]
Get:3 http://deb.devuan.org/merged daedalus/main ltspfs 1.5-2+devuan1 (diff) [8924 B]
dpkg-source: info: extracting ltspfs in ltspfs-1.5
dpkg-source: info: unpacking ltspfs_1.5.orig.tar.gz
dpkg-source: info: unpacking ltspfs_1.5-2+devuan1.debian.tar.xz
Fetched 48.6 kB in 0s (258 kB/s)
W: Download is performed unsandboxed as root as file 'ltspfs_1.5-2+devuan1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source ltspfs=1.5-2+devuan1
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  libblkid-dev libfuse-dev libglib2.0-bin libglib2.0-dev libglib2.0-dev-bin
  libmount-dev libselinux1-dev libsepol-dev uuid-dev
0 upgraded, 9 newly installed, 0 to remove and 0 not upgraded.
Need to get 3635 kB of archives.
After this operation, 21.6 MB of additional disk space will be used.
Get:1 http://deb.devuan.org/merged daedalus/main amd64 uuid-dev amd64 2.38.1-5devuan1+b1 [40.9 kB]
Get:2 http://deb.devuan.org/merged daedalus/main amd64 libblkid-dev amd64 2.38.1-5devuan1+b1 [180 kB]
Get:8 http://deb.devuan.org/merged daedalus/main amd64 libmount-dev amd64 2.38.1-5devuan1+b1 [23.4 kB]
Get:3 http://deb.devuan.org/merged daedalus/main amd64 libsepol-dev amd64 3.4-2.1 [351 kB]
Get:4 http://deb.devuan.org/merged daedalus/main amd64 libselinux1-dev amd64 3.4-1+b6 [157 kB]
Get:5 http://deb.devuan.org/merged daedalus/main amd64 libfuse-dev amd64 2.9.9-6+b1 [1021 kB]
Get:6 http://deb.devuan.org/merged daedalus/main amd64 libglib2.0-bin amd64 2.74.6-2 [110 kB]
Get:7 http://deb.devuan.org/merged daedalus/main amd64 libglib2.0-dev-bin amd64 2.74.6-2 [151 kB]
Get:9 http://deb.devuan.org/merged daedalus/main amd64 libglib2.0-dev amd64 2.74.6-2 [1601 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 3635 kB in 5s (779 kB/s)
Selecting previously unselected package uuid-dev:amd64.
(Reading database ... (Reading database ... 5%(Reading database ... 10%(Reading database ... 15%(Reading database ... 20%(Reading database ... 25%(Reading database ... 30%(Reading database ... 35%(Reading database ... 40%(Reading database ... 45%(Reading database ... 50%(Reading database ... 55%(Reading database ... 60%(Reading database ... 65%(Reading database ... 70%(Reading database ... 75%(Reading database ... 80%(Reading database ... 85%(Reading database ... 90%(Reading database ... 95%(Reading database ... 100%(Reading database ... 133311 files and directories currently installed.)
Preparing to unpack .../0-uuid-dev_2.38.1-5devuan1+b1_amd64.deb ...
Unpacking uuid-dev:amd64 (2.38.1-5devuan1+b1) ...
Selecting previously unselected package libblkid-dev:amd64.
Preparing to unpack .../1-libblkid-dev_2.38.1-5devuan1+b1_amd64.deb ...
Unpacking libblkid-dev:amd64 (2.38.1-5devuan1+b1) ...
Selecting previously unselected package libsepol-dev:amd64.
Preparing to unpack .../2-libsepol-dev_3.4-2.1_amd64.deb ...
Unpacking libsepol-dev:amd64 (3.4-2.1) ...
Selecting previously unselected package libselinux1-dev:amd64.
Preparing to unpack .../3-libselinux1-dev_3.4-1+b6_amd64.deb ...
Unpacking libselinux1-dev:amd64 (3.4-1+b6) ...
Selecting previously unselected package libfuse-dev:amd64.
Preparing to unpack .../4-libfuse-dev_2.9.9-6+b1_amd64.deb ...
Unpacking libfuse-dev:amd64 (2.9.9-6+b1) ...
Selecting previously unselected package libglib2.0-bin.
Preparing to unpack .../5-libglib2.0-bin_2.74.6-2_amd64.deb ...
Unpacking libglib2.0-bin (2.74.6-2) ...
Selecting previously unselected package libglib2.0-dev-bin.
Preparing to unpack .../6-libglib2.0-dev-bin_2.74.6-2_amd64.deb ...
Unpacking libglib2.0-dev-bin (2.74.6-2) ...
Selecting previously unselected package libmount-dev:amd64.
Preparing to unpack .../7-libmount-dev_2.38.1-5devuan1+b1_amd64.deb ...
Unpacking libmount-dev:amd64 (2.38.1-5devuan1+b1) ...
Selecting previously unselected package libglib2.0-dev:amd64.
Preparing to unpack .../8-libglib2.0-dev_2.74.6-2_amd64.deb ...
Unpacking libglib2.0-dev:amd64 (2.74.6-2) ...
Setting up libglib2.0-dev-bin (2.74.6-2) ...
Setting up libglib2.0-bin (2.74.6-2) ...
Setting up uuid-dev:amd64 (2.38.1-5devuan1+b1) ...
Setting up libsepol-dev:amd64 (3.4-2.1) ...
Setting up libblkid-dev:amd64 (2.38.1-5devuan1+b1) ...
Setting up libselinux1-dev:amd64 (3.4-1+b6) ...
Setting up libfuse-dev:amd64 (2.9.9-6+b1) ...
Setting up libmount-dev:amd64 (2.38.1-5devuan1+b1) ...
Setting up libglib2.0-dev:amd64 (2.74.6-2) ...
Processing triggers for man-db (2.11.2-2) ...
Processing triggers for libglib2.0-0:amd64 (2.74.6-2) ...
+ find . -maxdepth 1 -name ltspfs* -type d
+ cd ./ltspfs-1.5
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package ltspfs
dpkg-buildpackage: info: source version 1.5-2+devuan1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by Mark Hindley <mark@hindley.org.uk>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building ltspfs using existing ./ltspfs_1.5.orig.tar.gz
dpkg-source: info: building ltspfs in ltspfs_1.5-2+devuan1.debian.tar.xz
dpkg-source: info: building ltspfs in ltspfs_1.5-2+devuan1.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   debian/rules override_dh_autoreconf
make[1]: Entering directory '/build/ltspfs/ltspfs-1.5'
touch ChangeLog
dh_autoreconf
configure.ac:6: warning: 'AM_CONFIG_HEADER': this macro is obsolete.
configure.ac:6: You should use the 'AC_CONFIG_HEADERS' macro instead.
./lib/autoconf/general.m4:2434: AC_DIAGNOSE is expanded from...
aclocal.m4:1125: AM_CONFIG_HEADER is expanded from...
configure.ac:6: the top level
configure.ac:19: warning: The macro `AC_HEADER_STDC' is obsolete.
configure.ac:19: You should run autoupdate.
./lib/autoconf/headers.m4:704: AC_HEADER_STDC is expanded from...
configure.ac:19: the top level
configure.ac:9: installing './compile'
configure.ac:4: installing './install-sh'
configure.ac:4: installing './missing'
src/Makefile.am: installing './depcomp'
make[1]: Leaving directory '/build/ltspfs/ltspfs-1.5'
   dh_auto_configure
	./configure --build=x86_64-linux-gnu --prefix=/usr --includedir=\${prefix}/include --mandir=\${prefix}/share/man --infodir=\${prefix}/share/info --sysconfdir=/etc --localstatedir=/var --disable-option-checking --disable-silent-rules --libdir=\${prefix}/lib/x86_64-linux-gnu --libexecdir=\${prefix}/lib/x86_64-linux-gnu --runstatedir=/run --disable-maintainer-mode --disable-dependency-tracking
checking for a BSD-compatible install... /usr/bin/install -c
checking whether build environment is sane... yes
checking for a race-free mkdir -p... /bin/mkdir -p
checking for gawk... gawk
checking whether make sets $(MAKE)... yes
checking whether make supports nested variables... yes
checking whether to enable maintainer-specific portions of Makefiles... no
checking for gcc... gcc
checking whether the C compiler works... yes
checking for C compiler default output file name... a.out
checking for suffix of executables... 
checking whether we are cross compiling... no
checking for suffix of object files... o
checking whether the compiler supports GNU C... yes
checking whether gcc accepts -g... yes
checking for gcc option to enable C11 features... none needed
checking whether gcc understands -c and -o together... yes
checking whether make supports the include directive... yes (GNU style)
checking dependency style of gcc... none
checking for pkg-config... /usr/bin/pkg-config
checking pkg-config is at least version 0.9.0... yes
checking for fuse, x11... yes
checking for stdio.h... yes
checking for stdlib.h... yes
checking for string.h... yes
checking for inttypes.h... yes
checking for stdint.h... yes
checking for strings.h... yes
checking for sys/stat.h... yes
checking for sys/types.h... yes
checking for unistd.h... yes
checking for grep that handles long lines and -e... /bin/grep
checking for egrep... /bin/grep -E
checking for dirent.h that defines DIR... yes
checking for library containing opendir... none required
checking for sys/wait.h that is POSIX.1 compatible... yes
checking for fcntl.h... yes
checking for glib-2.0 >= 2.6.0... yes
checking for gobject-2.0 >= 2.6.0... yes
checking that generated files are newer than configure... done
configure: creating ./config.status
config.status: creating Makefile
config.status: creating src/Makefile
config.status: creating man/Makefile
config.status: creating scripts/Makefile
config.status: creating udev/Makefile
config.status: creating config.h
config.status: executing depfiles commands
   dh_auto_build
	make -j40
make[1]: Entering directory '/build/ltspfs/ltspfs-1.5'
make  all-recursive
make[2]: Entering directory '/build/ltspfs/ltspfs-1.5'
Making all in src
make[3]: Entering directory '/build/ltspfs/ltspfs-1.5/src'
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=22 -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfs-ltspfs.o `test -f 'ltspfs.c' || echo './'`ltspfs.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=22 -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfs-common.o `test -f 'common.c' || echo './'`common.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DAUTOMOUNT -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfsd-ltspfsd.o `test -f 'ltspfsd.c' || echo './'`ltspfsd.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DAUTOMOUNT -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfsd-ltspfsd_functions.o `test -f 'ltspfsd_functions.c' || echo './'`ltspfsd_functions.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2 -Wall -W -D_FILE_OFFSET_BITS=64 -DAUTOMOUNT -D_REENTRANT -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o ltspfsd-common.o `test -f 'common.c' || echo './'`common.c
gcc -DHAVE_CONFIG_H -I. -I..   -Wdate-time -D_FORTIFY_SOURCE=2  -g -O2 -ffile-prefix-map=/build/ltspfs/ltspfs-1.5=. -fstack-protector-strong -Wformat -Werror=format-security -c -o lbmount.o lbmount.c
ltspfsd.c:34:10: fatal error: rpc/xdr.h: No such file or directory
   34 | #include <rpc/xdr.h>
      |          ^~~~~~~~~~~
compilation terminated.
make[3]: *** [Makefile:442: ltspfsd-ltspfsd.o] Error 1
make[3]: *** Waiting for unfinished jobs....
ltspfsd_functions.c:33:10: fatal error: rpc/xdr.h: No such file or directory
   33 | #include <rpc/xdr.h>
      |          ^~~~~~~~~~~
compilation terminated.
make[3]: *** [Makefile:456: ltspfsd-ltspfsd_functions.o] Error 1
common.c:41:10: fatal error: rpc/xdr.h: No such file or directory
   41 | #include <rpc/xdr.h>
      |          ^~~~~~~~~~~
compilation terminated.
make[3]: *** [Makefile:428: ltspfs-common.o] Error 1
common.c:41:10: fatal error: rpc/xdr.h: No such file or directory
   41 | #include <rpc/xdr.h>
      |          ^~~~~~~~~~~
compilation terminated.
make[3]: *** [Makefile:470: ltspfsd-common.o] Error 1
ltspfs.c:37:10: fatal error: rpc/xdr.h: No such file or directory
   37 | #include <rpc/xdr.h>
      |          ^~~~~~~~~~~
compilation terminated.
make[3]: *** [Makefile:414: ltspfs-ltspfs.o] Error 1
make[3]: Leaving directory '/build/ltspfs/ltspfs-1.5/src'
make[2]: *** [Makefile:366: all-recursive] Error 1
make[2]: Leaving directory '/build/ltspfs/ltspfs-1.5'
make[1]: *** [Makefile:307: all] Error 2
make[1]: Leaving directory '/build/ltspfs/ltspfs-1.5'
dh_auto_build: error: make -j40 returned exit code 2
make: *** [debian/rules:7: build] Error 25
dpkg-buildpackage: error: debian/rules build subprocess returned exit status 2
