+ date
Wed Nov 29 08:59:38 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source devuan-baseconf=0.6.4+devuan3.2
Reading package lists...
NOTICE: 'devuan-baseconf' packaging is maintained in the 'Git' version control system at:
git://github.org/devuan/devuan-baseconf.git
Please use:
git clone git://github.org/devuan/devuan-baseconf.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 16.4 kB of source archives.
Get:1 http://deb.devuan.org/merged daedalus/main devuan-baseconf 0.6.4+devuan3.2 (dsc) [1318 B]
Get:2 http://deb.devuan.org/merged daedalus/main devuan-baseconf 0.6.4+devuan3.2 (tar) [15.1 kB]
dpkg-source: info: extracting devuan-baseconf in devuan-baseconf-0.6.4+devuan3.2
dpkg-source: info: unpacking devuan-baseconf_0.6.4+devuan3.2.tar.xz
Fetched 16.4 kB in 1s (19.0 kB/s)
W: Download is performed unsandboxed as root as file 'devuan-baseconf_0.6.4+devuan3.2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source devuan-baseconf=0.6.4+devuan3.2
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 3 not upgraded.
+ find . -maxdepth 1 -name devuan-baseconf* -type d
+ cd ./devuan-baseconf-0.6.4+devuan3.2
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym notrimdch dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package devuan-baseconf
dpkg-buildpackage: info: source version 0.6.4+devuan3.2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean 
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building devuan-baseconf in devuan-baseconf_0.6.4+devuan3.2.tar.xz
dpkg-source: info: building devuan-baseconf in devuan-baseconf_0.6.4+devuan3.2.dsc
 debian/rules build
dh build 
   dh_update_autotools_config
   dh_autoreconf
   debian/rules override_dh_auto_build
make[1]: Entering directory '/build/devuan-baseconf-0.6.4+devuan3.2'
sed "s/__DSUITE__/unstable/" debian/postinst > debian/postinst.tmp
mv debian/postinst.tmp debian/postinst
dh_auto_build
make[1]: Leaving directory '/build/devuan-baseconf-0.6.4+devuan3.2'
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary 
   dh_testroot
   dh_prep
   dh_installdirs
   dh_auto_install --destdir=debian/devuan-baseconf/
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'devuan-baseconf' in '../devuan-baseconf_0.6.4+devuan3.2_all.deb'.
 dpkg-genbuildinfo -O../devuan-baseconf_0.6.4+devuan3.2_amd64.buildinfo
 dpkg-genchanges -O../devuan-baseconf_0.6.4+devuan3.2_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Wed Nov 29 08:59:49 UTC 2023
+ cd ..
+ ls -la
total 56
drwxr-xr-x 3 root root  4096 Nov 29 08:59 .
dr-xr-xr-x 1 root root  4096 Nov 29 08:59 ..
-rw-r--r-- 1 root root  3272 Nov 29 08:59 buildlog.txt
drwxr-xr-x 4 root root  4096 Jan  5  2020 devuan-baseconf-0.6.4+devuan3.2
-rw-r--r-- 1 root root   780 Nov 29 08:59 devuan-baseconf_0.6.4+devuan3.2.dsc
-rw-r--r-- 1 root root 15136 Nov 29 08:59 devuan-baseconf_0.6.4+devuan3.2.tar.xz
-rw-r--r-- 1 root root  4200 Nov 29 08:59 devuan-baseconf_0.6.4+devuan3.2_all.deb
-rw-r--r-- 1 root root  5116 Nov 29 08:59 devuan-baseconf_0.6.4+devuan3.2_amd64.buildinfo
-rw-r--r-- 1 root root  1756 Nov 29 08:59 devuan-baseconf_0.6.4+devuan3.2_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./devuan-baseconf_0.6.4+devuan3.2_amd64.buildinfo ./devuan-baseconf_0.6.4+devuan3.2_amd64.changes ./devuan-baseconf_0.6.4+devuan3.2.dsc ./devuan-baseconf_0.6.4+devuan3.2.tar.xz ./devuan-baseconf_0.6.4+devuan3.2_all.deb
f2a1543970e7bccf9c2b21c5d3d78f8adb7665de702c523d892bda5470c79d99  ./buildlog.txt
df2f6bb4750e41f5aa463956a0b92048d40d447708b8c9be7acc4ff603fca103  ./devuan-baseconf_0.6.4+devuan3.2_amd64.buildinfo
72d1ef8721e98c7b708ff0fe1f856fc39e78cf65554cd45e22679d14863e316c  ./devuan-baseconf_0.6.4+devuan3.2_amd64.changes
783e4859434812858b089e659c4359d01721bcdb9488f8999a9e0c24efbcf42f  ./devuan-baseconf_0.6.4+devuan3.2.dsc
6f6db94252a8c483b99e09b808d546ca14660ebf185e52b26b023bdf3c156f2b  ./devuan-baseconf_0.6.4+devuan3.2.tar.xz
1ba9a7693f6068354b2f6279ff4639d232b6ac71a3d6b67cb328bfb893e44d2a  ./devuan-baseconf_0.6.4+devuan3.2_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls devuan-baseconf_0.6.4+devuan3.2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://deb.devuan.org/merged/pool/DEVUAN/main/d/devuan-baseconf/devuan-baseconf_0.6.4+devuan3.2_all.deb
--2023-11-29 08:59:49--  http://deb.devuan.org/merged/pool/DEVUAN/main/d/devuan-baseconf/devuan-baseconf_0.6.4+devuan3.2_all.deb
Resolving deb.devuan.org (deb.devuan.org)... 106.178.112.231, 160.16.137.156, 195.85.215.180, ...
Connecting to deb.devuan.org (deb.devuan.org)|106.178.112.231|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 4200 (4.1K) [application/vnd.debian.binary-package]
Saving to: 'devuan-baseconf_0.6.4+devuan3.2_all.deb'

     0K ....                                                  100%  139M=0s

2023-11-29 08:59:51 (139 MB/s) - 'devuan-baseconf_0.6.4+devuan3.2_all.deb' saved [4200/4200]

+ + find . -maxdepth 1 -type f
tee ../SHA256SUMS
+ sha256sum ./devuan-baseconf_0.6.4+devuan3.2_all.deb
1ba9a7693f6068354b2f6279ff4639d232b6ac71a3d6b67cb328bfb893e44d2a  ./devuan-baseconf_0.6.4+devuan3.2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./devuan-baseconf_0.6.4+devuan3.2_all.deb: OK
+ echo Package devuan-baseconf version 0.6.4+devuan3.2 is reproducible!
Package devuan-baseconf version 0.6.4+devuan3.2 is reproducible!
