+ date
Sun Nov 26 12:15:14 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source darkpurpy-icon-theme=1.1
Reading package lists...
Need to get 854 kB of source archives.
Get:1 http://deb.devuan.org/merged daedalus/main darkpurpy-icon-theme 1.1 (dsc) [1072 B]
Get:2 http://deb.devuan.org/merged daedalus/main darkpurpy-icon-theme 1.1 (tar) [853 kB]
dpkg-source: info: extracting darkpurpy-icon-theme in darkpurpy-icon-theme-1.1
dpkg-source: info: unpacking darkpurpy-icon-theme_1.1.tar.gz
Fetched 854 kB in 2s (392 kB/s)
W: Download is performed unsandboxed as root as file 'darkpurpy-icon-theme_1.1.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source darkpurpy-icon-theme=1.1
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name darkpurpy-icon-theme* -type d
+ cd ./darkpurpy-icon-theme-1.1
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package darkpurpy-icon-theme
dpkg-buildpackage: info: source version 1.1
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh_testdir
dh_clean
 dpkg-source -b .
dpkg-source: warning: no source format specified in debian/source/format, see dpkg-source(1)
dpkg-source: info: using source format '1.0'
dpkg-source: info: building darkpurpy-icon-theme in darkpurpy-icon-theme_1.1.tar.gz
dpkg-source: warning: missing information for output field Standards-Version
dpkg-source: info: building darkpurpy-icon-theme in darkpurpy-icon-theme_1.1.dsc
 debian/rules build
make: Nothing to be done for 'build'.
 debian/rules binary
dh_testdir
dh_testroot
dh_clean -k
dh_clean: warning: dh_clean -k is deprecated; use dh_prep instead
dh_clean: warning: This feature will be removed in compat 12.
mkdir -p debian/darkpurpy-icon-theme
cp -ra files/* debian/darkpurpy-icon-theme
dh_installdocs -pdarkpurpy-icon-theme
dh_installchangelogs -pdarkpurpy-icon-theme
dh_compress
dh_fixperms
dh_strip
dh_makeshlibs
dh_shlibdeps
dh_installdeb
dh_gencontrol
dh_md5sums
dh_builddeb
dpkg-deb: building package 'darkpurpy-icon-theme' in '../darkpurpy-icon-theme_1.1_all.deb'.
 dpkg-genbuildinfo -O../darkpurpy-icon-theme_1.1_amd64.buildinfo
 dpkg-genchanges -O../darkpurpy-icon-theme_1.1_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Nov 26 12:15:22 UTC 2023
+ cd ..
+ ls -la
total 1544
drwxr-xr-x 3 root root   4096 Nov 26 12:15 .
drwxr-xr-x 3 root root   4096 Nov 26 12:15 ..
-rw-r--r-- 1 root root   2831 Nov 26 12:15 buildlog.txt
drwxr-xr-x 4 root root   4096 Aug 17  2021 darkpurpy-icon-theme-1.1
-rw-r--r-- 1 root root    534 Nov 26 12:15 darkpurpy-icon-theme_1.1.dsc
-rw-r--r-- 1 root root 858035 Nov 26 12:15 darkpurpy-icon-theme_1.1.tar.gz
-rw-r--r-- 1 root root 685976 Nov 26 12:15 darkpurpy-icon-theme_1.1_all.deb
-rw-r--r-- 1 root root   5082 Nov 26 12:15 darkpurpy-icon-theme_1.1_amd64.buildinfo
-rw-r--r-- 1 root root   1588 Nov 26 12:15 darkpurpy-icon-theme_1.1_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./buildlog.txt ./darkpurpy-icon-theme_1.1_amd64.buildinfo ./darkpurpy-icon-theme_1.1_all.deb ./darkpurpy-icon-theme_1.1_amd64.changes ./darkpurpy-icon-theme_1.1.dsc ./darkpurpy-icon-theme_1.1.tar.gz
3e34bfca81fc9d67d47f7bfec79603cfb359b1e869e4095294208b26e506365b  ./buildlog.txt
36846ecb9065337b7ddf6dd30f37bbbc7c2b9a9fae6eaa20e2bde9c8be3095d9  ./darkpurpy-icon-theme_1.1_amd64.buildinfo
6caeabf99cfa97567ad9bb4b10d192b2543847fedd615be72a91b6c9cd041ab2  ./darkpurpy-icon-theme_1.1_all.deb
015d4eb39f27908a38ee42e0c8057653bfe65605ed645569d554bd572c846eff  ./darkpurpy-icon-theme_1.1_amd64.changes
11a673bea4d8bfe0ed620042f15b2d9be85a023ba3c9777cfea144db86c3e3f0  ./darkpurpy-icon-theme_1.1.dsc
3f374fca2fd1a70d0e471c48db81f894221f75c1070af4a2e1d8aab45160bab2  ./darkpurpy-icon-theme_1.1.tar.gz
+ mkdir published
+ cd published
+ cd ../
+ ls darkpurpy-icon-theme_1.1_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://deb.devuan.org/merged/pool/DEVUAN/main/d/darkpurpy-icon-theme/darkpurpy-icon-theme_1.1_all.deb
--2023-11-26 12:15:22--  http://deb.devuan.org/merged/pool/DEVUAN/main/d/darkpurpy-icon-theme/darkpurpy-icon-theme_1.1_all.deb
Resolving deb.devuan.org (deb.devuan.org)... 185.38.15.84, 103.146.168.12, 106.178.112.231, ...
Connecting to deb.devuan.org (deb.devuan.org)|185.38.15.84|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 685976 (670K) [application/x-debian-package]
Saving to: 'darkpurpy-icon-theme_1.1_all.deb'

     0K .......... .......... .......... .......... ..........  7% 1.07M 1s
    50K .......... .......... .......... .......... .......... 14% 2.17M 0s
   100K .......... .......... .......... .......... .......... 22% 22.5M 0s
   150K .......... .......... .......... .......... .......... 29% 19.0M 0s
   200K .......... .......... .......... .......... .......... 37% 2.44M 0s
   250K .......... .......... .......... .......... .......... 44% 25.5M 0s
   300K .......... .......... .......... .......... .......... 52% 25.8M 0s
   350K .......... .......... .......... .......... .......... 59% 25.9M 0s
   400K .......... .......... .......... .......... .......... 67% 2.91M 0s
   450K .......... .......... .......... .......... .......... 74% 25.7M 0s
   500K .......... .......... .......... .......... .......... 82% 21.6M 0s
   550K .......... .......... .......... .......... .......... 89% 25.4M 0s
   600K .......... .......... .......... .......... .......... 97% 25.4M 0s
   650K .......... .........                                  100% 19.5M=0.1s

2023-11-26 12:15:22 (5.26 MB/s) - 'darkpurpy-icon-theme_1.1_all.deb' saved [685976/685976]

+ + tee ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./darkpurpy-icon-theme_1.1_all.deb
6caeabf99cfa97567ad9bb4b10d192b2543847fedd615be72a91b6c9cd041ab2  ./darkpurpy-icon-theme_1.1_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./darkpurpy-icon-theme_1.1_all.deb: OK
+ echo Package darkpurpy-icon-theme version 1.1 is reproducible!
Package darkpurpy-icon-theme version 1.1 is reproducible!
