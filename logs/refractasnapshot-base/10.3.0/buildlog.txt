+ date
Sun Nov 26 15:06:00 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source refractasnapshot-base=10.3.0
Reading package lists...
NOTICE: 'refractasnapshot-base' packaging is maintained in the 'Git' version control system at:
git@git.devuan.org:devuan/refractasnapshot-base.git
Please use:
git clone git@git.devuan.org:devuan/refractasnapshot-base.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 145 kB of source archives.
Get:1 http://deb.devuan.org/merged daedalus/main refractasnapshot-base 10.3.0 (dsc) [1273 B]
Get:2 http://deb.devuan.org/merged daedalus/main refractasnapshot-base 10.3.0 (tar) [144 kB]
dpkg-source: info: extracting refractasnapshot-base in refractasnapshot-base-10.3.0
dpkg-source: info: unpacking refractasnapshot-base_10.3.0.tar.xz
Fetched 145 kB in 2s (91.4 kB/s)
W: Download is performed unsandboxed as root as file 'refractasnapshot-base_10.3.0.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source refractasnapshot-base=10.3.0
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
+ find . -maxdepth 1 -name refractasnapshot-base* -type d
+ cd ./refractasnapshot-base-10.3.0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package refractasnapshot-base
dpkg-buildpackage: info: source version 10.3.0
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
chmod 0655 "/build/refractasnapshot-base/refractasnapshot-base-10.3.0/refractasnapshot"
dh clean 
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (native)'
dpkg-source: info: building refractasnapshot-base in refractasnapshot-base_10.3.0.tar.xz
dpkg-source: info: building refractasnapshot-base in refractasnapshot-base_10.3.0.dsc
 debian/rules build
chmod 0655 "/build/refractasnapshot-base/refractasnapshot-base-10.3.0/refractasnapshot"
dh build 
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
chmod 0655 "/build/refractasnapshot-base/refractasnapshot-base-10.3.0/refractasnapshot"
dh binary 
   dh_testroot
   dh_prep
   dh_auto_install --destdir=debian/refractasnapshot-base/
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   dh_fixperms
   dh_missing
   dh_installdeb
   dh_gencontrol
dpkg-gencontrol: warning: Depends field of package refractasnapshot-base: substitution variable ${shlibs:Depends} used, but is not defined
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'refractasnapshot-base' in '../refractasnapshot-base_10.3.0_all.deb'.
 dpkg-genbuildinfo -O../refractasnapshot-base_10.3.0_amd64.buildinfo
 dpkg-genchanges -O../refractasnapshot-base_10.3.0_amd64.changes
dpkg-genchanges: info: including full source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: full upload; Debian-native package (full source is included)
+ date
Sun Nov 26 15:06:10 UTC 2023
+ cd ..
+ ls -la
total 320
drwxr-xr-x 3 root root   4096 Nov 26 15:06 .
drwxr-xr-x 3 root root   4096 Nov 26 15:06 ..
-rw-r--r-- 1 root root   3364 Nov 26 15:06 buildlog.txt
drwxr-xr-x 5 root root   4096 Jun  3 15:11 refractasnapshot-base-10.3.0
-rw-r--r-- 1 root root    735 Nov 26 15:06 refractasnapshot-base_10.3.0.dsc
-rw-r--r-- 1 root root 144164 Nov 26 15:06 refractasnapshot-base_10.3.0.tar.xz
-rw-r--r-- 1 root root 145644 Nov 26 15:06 refractasnapshot-base_10.3.0_all.deb
-rw-r--r-- 1 root root   5116 Nov 26 15:06 refractasnapshot-base_10.3.0_amd64.buildinfo
-rw-r--r-- 1 root root   1825 Nov 26 15:06 refractasnapshot-base_10.3.0_amd64.changes
+ find . -maxdepth 1 -type f
+ sha256sum ./refractasnapshot-base_10.3.0_amd64.buildinfo ./buildlog.txt ./refractasnapshot-base_10.3.0_amd64.changes ./refractasnapshot-base_10.3.0_all.deb ./refractasnapshot-base_10.3.0.dsc ./refractasnapshot-base_10.3.0.tar.xz
49aa86ba2f11728709e0310505fa7fc2b4da65e93ae5fb0afc9e12a22af2e254  ./refractasnapshot-base_10.3.0_amd64.buildinfo
48a2b9908197b1f3d3fef6078d15db4219001610f7c5a187df7dcc5917769ad0  ./buildlog.txt
76ef9a6974b0db975d170f7a55c846d56e635819ee8c8da63dc230c53c7ad1c1  ./refractasnapshot-base_10.3.0_amd64.changes
c11667e01067a93cc45ef1934fd4cebb4fea199c1aa55d0dc8a4402a371e95a1  ./refractasnapshot-base_10.3.0_all.deb
a392404150e4d4bdbe74ce9c6cb3b0104137c894d159c8147479975a3645c943  ./refractasnapshot-base_10.3.0.dsc
c52c87933a80ecace1adf5b0537e7f0477152d055f5bc5edd7b0977501136014  ./refractasnapshot-base_10.3.0.tar.xz
+ mkdir published
+ cd published
+ cd ../
+ ls refractasnapshot-base_10.3.0_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://deb.devuan.org/merged/pool/DEVUAN/main/r/refractasnapshot-base/refractasnapshot-base_10.3.0_all.deb
--2023-11-26 15:06:10--  http://deb.devuan.org/merged/pool/DEVUAN/main/r/refractasnapshot-base/refractasnapshot-base_10.3.0_all.deb
Resolving deb.devuan.org (deb.devuan.org)... 89.174.102.150, 95.216.15.86, 46.4.50.2, ...
Connecting to deb.devuan.org (deb.devuan.org)|89.174.102.150|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 145644 (142K) [application/octet-stream]
Saving to: 'refractasnapshot-base_10.3.0_all.deb'

     0K .......... .......... .......... .......... .......... 35%  716K 0s
    50K .......... .......... .......... .......... .......... 70% 1.41M 0s
   100K .......... .......... .......... .......... ..        100% 19.2M=0.1s

2023-11-26 15:06:10 (1.30 MB/s) - 'refractasnapshot-base_10.3.0_all.deb' saved [145644/145644]

+ + tee ../SHA256SUMS
find . -maxdepth 1 -type f
+ sha256sum ./refractasnapshot-base_10.3.0_all.deb
c11667e01067a93cc45ef1934fd4cebb4fea199c1aa55d0dc8a4402a371e95a1  ./refractasnapshot-base_10.3.0_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./refractasnapshot-base_10.3.0_all.deb: OK
+ echo Package refractasnapshot-base version 10.3.0 is reproducible!
Package refractasnapshot-base version 10.3.0 is reproducible!
