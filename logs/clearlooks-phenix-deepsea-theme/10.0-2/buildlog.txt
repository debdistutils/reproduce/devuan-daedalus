+ date
Wed Nov 29 08:58:00 UTC 2023
+ id
uid=0(root) gid=0(root) groups=0(root)
+ apt-get source --only-source clearlooks-phenix-deepsea-theme=10.0-2
Reading package lists...
NOTICE: 'clearlooks-phenix-deepsea-theme' packaging is maintained in the 'Git' version control system at:
https://git.devuan.org/devuan/clearlooks-phenix-deepsea-theme.git
Please use:
git clone https://git.devuan.org/devuan/clearlooks-phenix-deepsea-theme.git
to retrieve the latest (possibly unreleased) updates to the package.
Need to get 331 kB of source archives.
Get:1 http://deb.devuan.org/merged daedalus/main clearlooks-phenix-deepsea-theme 10.0-2 (dsc) [1738 B]
Get:2 http://deb.devuan.org/merged daedalus/main clearlooks-phenix-deepsea-theme 10.0-2 (tar) [324 kB]
Get:3 http://deb.devuan.org/merged daedalus/main clearlooks-phenix-deepsea-theme 10.0-2 (diff) [4568 B]
dpkg-source: info: extracting clearlooks-phenix-deepsea-theme in clearlooks-phenix-deepsea-theme-10.0
dpkg-source: info: unpacking clearlooks-phenix-deepsea-theme_10.0.orig.tar.gz
dpkg-source: info: unpacking clearlooks-phenix-deepsea-theme_10.0-2.debian.tar.xz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: applying fix-rubberband.diff
Fetched 331 kB in 2s (161 kB/s)
W: Download is performed unsandboxed as root as file 'clearlooks-phenix-deepsea-theme_10.0-2.dsc' couldn't be accessed by user '_apt'. - pkgAcquire::Run (13: Permission denied)
+ env DEBIAN_FRONTEND=noninteractive apt-get build-dep -y --only-source clearlooks-phenix-deepsea-theme=10.0-2
Reading package lists...
Building dependency tree...
Reading state information...
0 upgraded, 0 newly installed, 0 to remove and 3 not upgraded.
+ find . -maxdepth 1 -name clearlooks-phenix-deepsea-theme* -type d
+ cd ./clearlooks-phenix-deepsea-theme-10.0
+ eatmydata env DEB_BUILD_OPTIONS=noautodbgsym notrimdch dpkg-buildpackage --no-sign
dpkg-buildpackage: info: source package clearlooks-phenix-deepsea-theme
dpkg-buildpackage: info: source version 10.0-2
dpkg-buildpackage: info: source distribution unstable
dpkg-buildpackage: info: source changed by fsmithred <fsmithred@gmail.com>
 dpkg-source --before-build .
dpkg-buildpackage: info: host architecture amd64
 debian/rules clean
dh clean
   dh_clean
 dpkg-source -b .
dpkg-source: info: using source format '3.0 (quilt)'
dpkg-source: info: building clearlooks-phenix-deepsea-theme using existing ./clearlooks-phenix-deepsea-theme_10.0.orig.tar.gz
dpkg-source: info: using patch list from debian/patches/series
dpkg-source: info: building clearlooks-phenix-deepsea-theme in clearlooks-phenix-deepsea-theme_10.0-2.debian.tar.xz
dpkg-source: info: building clearlooks-phenix-deepsea-theme in clearlooks-phenix-deepsea-theme_10.0-2.dsc
 debian/rules build
dh build
   dh_update_autotools_config
   dh_autoreconf
   create-stamp debian/debhelper-build-stamp
 debian/rules binary
dh binary
   dh_testroot
   dh_prep
   dh_auto_install --destdir=debian/clearlooks-phenix-deepsea-theme/
   dh_install
   dh_installdocs
   dh_installchangelogs
   dh_perl
   dh_link
   dh_strip_nondeterminism
   dh_compress
   debian/rules override_dh_fixperms
make[1]: Entering directory '/build/clearlooks-phenix-deepsea-theme-10.0'
dh_fixperms
find debian/*/usr/share/themes -type f -print0 2>/dev/null | xargs -0r chmod 644
make[1]: Leaving directory '/build/clearlooks-phenix-deepsea-theme-10.0'
   dh_missing
   dh_installdeb
   dh_gencontrol
   dh_md5sums
   dh_builddeb
dpkg-deb: building package 'clearlooks-phenix-deepsea-theme' in '../clearlooks-phenix-deepsea-theme_10.0-2_all.deb'.
 dpkg-genbuildinfo -O../clearlooks-phenix-deepsea-theme_10.0-2_amd64.buildinfo
 dpkg-genchanges -O../clearlooks-phenix-deepsea-theme_10.0-2_amd64.changes
dpkg-genchanges: info: not including original source code in upload
 dpkg-source --after-build .
dpkg-buildpackage: info: binary and diff upload (original source NOT included)
+ date
Wed Nov 29 08:58:11 UTC 2023
+ cd ..
+ ls -la
total 452
drwxr-xr-x  3 root root   4096 Nov 29 08:58 .
dr-xr-xr-x  1 root root   4096 Nov 29 08:58 ..
-rw-r--r--  1 root root   3959 Nov 29 08:58 buildlog.txt
drwxr-xr-x 12 root root   4096 Nov 29 08:58 clearlooks-phenix-deepsea-theme-10.0
-rw-r--r--  1 root root   4576 Nov 29 08:58 clearlooks-phenix-deepsea-theme_10.0-2.debian.tar.xz
-rw-r--r--  1 root root   1200 Nov 29 08:58 clearlooks-phenix-deepsea-theme_10.0-2.dsc
-rw-r--r--  1 root root  91744 Nov 29 08:58 clearlooks-phenix-deepsea-theme_10.0-2_all.deb
-rw-r--r--  1 root root   5192 Nov 29 08:58 clearlooks-phenix-deepsea-theme_10.0-2_amd64.buildinfo
-rw-r--r--  1 root root   1866 Nov 29 08:58 clearlooks-phenix-deepsea-theme_10.0-2_amd64.changes
-rw-r--r--  1 root root 324482 Mar 21  2021 clearlooks-phenix-deepsea-theme_10.0.orig.tar.gz
+ find . -maxdepth 1 -type f
+ sha256sum ./clearlooks-phenix-deepsea-theme_10.0-2.dsc ./clearlooks-phenix-deepsea-theme_10.0.orig.tar.gz ./buildlog.txt ./clearlooks-phenix-deepsea-theme_10.0-2.debian.tar.xz ./clearlooks-phenix-deepsea-theme_10.0-2_amd64.buildinfo ./clearlooks-phenix-deepsea-theme_10.0-2_amd64.changes ./clearlooks-phenix-deepsea-theme_10.0-2_all.deb
7ea3078fdbbe48b088918258e92ad58ac19cb0dc3c274b74d107c0a572d48fd9  ./clearlooks-phenix-deepsea-theme_10.0-2.dsc
eb6e9606194b169cf41181910249364e269a63cb6104eecb1271b2b3cb2b0043  ./clearlooks-phenix-deepsea-theme_10.0.orig.tar.gz
5df9c0b1816389ee84313c4f9adb9be4dfeb2626f34b3d5976d9c78e98301a6e  ./buildlog.txt
aea6e475e363f7cb1d07205fb7afa47de35fc5a76621ab2e1b3627f012b50ac4  ./clearlooks-phenix-deepsea-theme_10.0-2.debian.tar.xz
b0f6deca5fd209c33ceaf3eb3e49d363279abed9aa1f1fc1f9dc83f702acad2b  ./clearlooks-phenix-deepsea-theme_10.0-2_amd64.buildinfo
d8a6b60beaa6316eb869804a83a1363b568623390008209ef98928e23c5c7d56  ./clearlooks-phenix-deepsea-theme_10.0-2_amd64.changes
631ecea2811298b88fb2b256e092e90580fae5ede37dee53cba642411b04c16d  ./clearlooks-phenix-deepsea-theme_10.0-2_all.deb
+ mkdir published
+ cd published
+ cd ../
+ ls clearlooks-phenix-deepsea-theme_10.0-2_all.deb *.udeb
ls: cannot access '*.udeb': No such file or directory
+ wget --retry-connrefused --retry-on-host-error --tries=3 http://deb.devuan.org/merged/pool/DEVUAN/main/c/clearlooks-phenix-deepsea-theme/clearlooks-phenix-deepsea-theme_10.0-2_all.deb
--2023-11-29 08:58:11--  http://deb.devuan.org/merged/pool/DEVUAN/main/c/clearlooks-phenix-deepsea-theme/clearlooks-phenix-deepsea-theme_10.0-2_all.deb
Resolving deb.devuan.org (deb.devuan.org)... 106.178.112.231, 89.174.102.150, 103.146.168.12, ...
Connecting to deb.devuan.org (deb.devuan.org)|106.178.112.231|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 91744 (90K) [application/vnd.debian.binary-package]
Saving to: 'clearlooks-phenix-deepsea-theme_10.0-2_all.deb'

     0K .......... .......... .......... .......... .......... 55% 16.9K 2s
    50K .......... .......... .......... .........            100% 19.4K=5.0s

2023-11-29 08:58:18 (17.9 KB/s) - 'clearlooks-phenix-deepsea-theme_10.0-2_all.deb' saved [91744/91744]

+ tee ../SHA256SUMS
+ find . -maxdepth 1 -type f
+ sha256sum ./clearlooks-phenix-deepsea-theme_10.0-2_all.deb
631ecea2811298b88fb2b256e092e90580fae5ede37dee53cba642411b04c16d  ./clearlooks-phenix-deepsea-theme_10.0-2_all.deb
+ cd ..
+ sha256sum -c SHA256SUMS
./clearlooks-phenix-deepsea-theme_10.0-2_all.deb: OK
+ echo Package clearlooks-phenix-deepsea-theme version 10.0-2 is reproducible!
Package clearlooks-phenix-deepsea-theme version 10.0-2 is reproducible!
